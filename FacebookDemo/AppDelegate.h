//
//  AppDelegate.h
//  FacebookDemo
//
//  Created by M R on 4/24/17.
//  Copyright © 2017 M R. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


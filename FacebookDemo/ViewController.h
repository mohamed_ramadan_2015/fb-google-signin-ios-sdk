//
//  ViewController.h
//  FacebookDemo
//
//  Created by M R on 4/24/17.
//  Copyright © 2017 M R. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>
#import "FBSDKCoreKit/FBSDKCoreKit.h"
#import "FBSDKLoginKit/FBSDKLoginKit.h"

@interface ViewController : UIViewController <FBSDKLoginButtonDelegate,UITextFieldDelegate, GIDSignInUIDelegate,GIDSignInDelegate>


@end


//
//  ViewController.m
//  FacebookDemo
//
//  Created by M R on 4/24/17.
//  Copyright © 2017 M R. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation ViewController
- (IBAction)didGoogleSignOut:(id)sender {
    [[GIDSignIn sharedInstance] signOut];
    NSLog(@"User SignedOut");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    
    [[GIDSignIn sharedInstance]signInSilently];
    
    if ([FBSDKAccessToken currentAccessToken]){
        [_fbButton setTitle:@"FB Logout" forState:UIControlStateNormal];
    }else{
        [_fbButton setTitle:@"FB Login" forState:UIControlStateNormal];
    }
    /*
     FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc]init];
     loginButton.delegate = self;
     loginButton.center = self.view.center;
     loginButton.readPermissions = @[@"public_profile",@"email",@"user_friends"];
     [self.view addSubview:loginButton];
     */
    [self fetchFbUserInfo];
}

-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{
    if(error){
        NSLog(@"Google SignIn Error: %@",[error description]);
    }else{
        NSLog(@"Google user SignedIn Successfully");
        NSString *userId = user.userID;                  // For client-side use only!
        //NSString *idToken = user.authentication.idToken; // Safe to send to the server
        NSString *fullName = user.profile.name;
        NSString *givenName = user.profile.givenName;
        NSString *familyName = user.profile.familyName;
        NSString *email = user.profile.email;
        
        NSLog(@"UserId = %@",userId);
        //NSLog(@"idToken = %@",idToken);
        NSLog(@"fullName = %@",fullName);
        NSLog(@"givenName = %@",givenName);
        NSLog(@"familyName = %@",familyName);
        NSLog(@"email = %@",email);
    }
}
-(void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton{
    NSLog(@"User Logged out");
}

-(void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error{
    
    if (error) {
        NSLog(@"FB error: %@",[error description]);
        return;
    }
    
    if([FBSDKAccessToken currentAccessToken]){
        NSLog(@"Result: %@",result);
        [self fetchFbUserInfo];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)tapon_facebookLogin:(id)sender {
    if ([FBSDKAccessToken currentAccessToken]) {
        // TODO:Token is already available.
        NSLog(@"FBSDKAccessToken alreay exist");
        [self fetchFbUserInfo];
        
    }else{
        NSLog(@"FBSDKAccessToken not exist");
        FBSDKLoginManager *loginManager = [[FBSDKLoginManager alloc] init];
        [loginManager logInWithReadPermissions:@[@"email",@"public_profile"]
                            fromViewController:self
                                       handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                           //TODO: process error or result
                                           if (!error) {
                                               NSLog(@"result %@",result.debugDescription);
                                               [self fetchFbUserInfo];
                                           }else{
                                               NSLog(@"errorfacebook %@",error.description);
                                           }
                                       }];
    }
}

-(void)fetchFbUserInfo{
    
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"id, name, link, first_name, last_name, picture.type(large), email, birthday ,location ,friends ,hometown , friendlists"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 NSLog(@"resultisfetchFbUserInfo:%@",result);
                 NSLog(@"User_id: %@",[result valueForKey:@"id"]);
             }
             else
             {
                 NSLog(@"ErrorfetchFbUserInfo %@",error);
             }
         }];}
}

#pragma mark text field delegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [self animateTextField:textField up:YES];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view resignFirstResponder];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -30; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end

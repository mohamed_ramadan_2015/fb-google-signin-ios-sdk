//
//  main.m
//  FacebookDemo
//
//  Created by M R on 4/24/17.
//  Copyright © 2017 M R. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
